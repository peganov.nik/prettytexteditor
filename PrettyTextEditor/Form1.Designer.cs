﻿
using System;
using System.Drawing;

namespace PrettyTextEditor
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.upToolStrip = new System.Windows.Forms.ToolStrip();
            this.fileStripDropDownButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.newStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editStripDropDownButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.selectStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cutStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.formatStripDropDownButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.italicStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.boldStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.underlineStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.strikethroughStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.fontStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsStripDropDownButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.autoSaveToolStripMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.colorStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hotKeysStripButton = new System.Windows.Forms.ToolStripButton();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.selectContextStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cutContextStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyContextStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteContextStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.italicContextStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.boldContextStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.underlineContextStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.strikethroughContextStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.upToolStrip.SuspendLayout();
            this.contextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // upToolStrip
            // 
            this.upToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileStripDropDownButton,
            this.editStripDropDownButton,
            this.formatStripDropDownButton,
            this.settingsStripDropDownButton,
            this.hotKeysStripButton});
            this.upToolStrip.Location = new System.Drawing.Point(0, 0);
            this.upToolStrip.Name = "upToolStrip";
            this.upToolStrip.Size = new System.Drawing.Size(810, 25);
            this.upToolStrip.TabIndex = 0;
            this.upToolStrip.Text = "toolStrip1";
            // 
            // fileStripDropDownButton
            // 
            this.fileStripDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.fileStripDropDownButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newStripMenuItem,
            this.openStripMenuItem,
            this.saveStripMenuItem,
            this.saveAsStripMenuItem});
            this.fileStripDropDownButton.Image = ((System.Drawing.Image)(resources.GetObject("fileStripDropDownButton.Image")));
            this.fileStripDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.fileStripDropDownButton.Name = "fileStripDropDownButton";
            this.fileStripDropDownButton.Size = new System.Drawing.Size(38, 22);
            this.fileStripDropDownButton.Text = "File";
            // 
            // newStripMenuItem
            // 
            this.newStripMenuItem.Name = "newStripMenuItem";
            this.newStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.newStripMenuItem.Text = "&New file";
            this.newStripMenuItem.Click += new System.EventHandler(this.NewFile);
            // 
            // openStripMenuItem
            // 
            this.openStripMenuItem.Name = "openStripMenuItem";
            this.openStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.openStripMenuItem.Text = "Open";
            this.openStripMenuItem.Click += new System.EventHandler(this.OpenFile);
            // 
            // saveStripMenuItem
            // 
            this.saveStripMenuItem.Name = "saveStripMenuItem";
            this.saveStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.saveStripMenuItem.Text = "Save";
            this.saveStripMenuItem.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.saveStripMenuItem.Click += new System.EventHandler(this.SaveFile);
            // 
            // saveAsStripMenuItem
            // 
            this.saveAsStripMenuItem.Name = "saveAsStripMenuItem";
            this.saveAsStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.saveAsStripMenuItem.Text = "Save as";
            this.saveAsStripMenuItem.Click += new System.EventHandler(this.SaveAs);
            // 
            // editStripDropDownButton
            // 
            this.editStripDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.editStripDropDownButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectStripMenuItem,
            this.cutStripMenuItem,
            this.copyStripMenuItem,
            this.pasteStripMenuItem});
            this.editStripDropDownButton.Image = ((System.Drawing.Image)(resources.GetObject("editStripDropDownButton.Image")));
            this.editStripDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.editStripDropDownButton.Name = "editStripDropDownButton";
            this.editStripDropDownButton.Size = new System.Drawing.Size(40, 22);
            this.editStripDropDownButton.Text = "Edit";
            // 
            // selectStripMenuItem
            // 
            this.selectStripMenuItem.Name = "selectStripMenuItem";
            this.selectStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.selectStripMenuItem.Text = "Select all";
            // 
            // cutStripMenuItem
            // 
            this.cutStripMenuItem.Name = "cutStripMenuItem";
            this.cutStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.cutStripMenuItem.Text = "Cut";
            // 
            // copyStripMenuItem
            // 
            this.copyStripMenuItem.Name = "copyStripMenuItem";
            this.copyStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.copyStripMenuItem.Text = "Copy";
            // 
            // pasteStripMenuItem
            // 
            this.pasteStripMenuItem.Name = "pasteStripMenuItem";
            this.pasteStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.pasteStripMenuItem.Text = "Paste";
            // 
            // formatStripDropDownButton
            // 
            this.formatStripDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.formatStripDropDownButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.italicStripMenuItem,
            this.boldStripMenuItem,
            this.underlineStripMenuItem,
            this.strikethroughStripMenuItem,
            this.toolStripSeparator1,
            this.fontStripMenuItem});
            this.formatStripDropDownButton.Image = ((System.Drawing.Image)(resources.GetObject("formatStripDropDownButton.Image")));
            this.formatStripDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.formatStripDropDownButton.Name = "formatStripDropDownButton";
            this.formatStripDropDownButton.Size = new System.Drawing.Size(58, 22);
            this.formatStripDropDownButton.Text = "Format";
            // 
            // italicStripMenuItem
            // 
            this.italicStripMenuItem.Name = "italicStripMenuItem";
            this.italicStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.italicStripMenuItem.Text = "italic";
            // 
            // boldStripMenuItem
            // 
            this.boldStripMenuItem.Name = "boldStripMenuItem";
            this.boldStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.boldStripMenuItem.Text = "bold";
            // 
            // underlineStripMenuItem
            // 
            this.underlineStripMenuItem.Name = "underlineStripMenuItem";
            this.underlineStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.underlineStripMenuItem.Text = "underline";
            // 
            // strikethroughStripMenuItem
            // 
            this.strikethroughStripMenuItem.Name = "strikethroughStripMenuItem";
            this.strikethroughStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.strikethroughStripMenuItem.Text = "strikethrough";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(142, 6);
            // 
            // fontStripMenuItem
            // 
            this.fontStripMenuItem.Name = "fontStripMenuItem";
            this.fontStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.fontStripMenuItem.Text = "Font";
            this.fontStripMenuItem.Click += FontMenu;
            // 
            // settingsStripDropDownButton
            // 
            this.settingsStripDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.settingsStripDropDownButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.autoSaveToolStripMenu,
            this.colorStripMenuItem});
            this.settingsStripDropDownButton.Image = ((System.Drawing.Image)(resources.GetObject("settingsStripDropDownButton.Image")));
            this.settingsStripDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.settingsStripDropDownButton.Name = "settingsStripDropDownButton";
            this.settingsStripDropDownButton.Size = new System.Drawing.Size(62, 22);
            this.settingsStripDropDownButton.Text = "Settings";
            // 
            // autoSaveToolStripMenu
            // 
            this.autoSaveToolStripMenu.Name = "autoSaveToolStripMenu";
            this.autoSaveToolStripMenu.Size = new System.Drawing.Size(147, 22);
            this.autoSaveToolStripMenu.Text = "Auto saving";
            this.autoSaveToolStripMenu.Click += new System.EventHandler(this.AutoSaveMenu);
            // 
            // colorStripMenuItem
            // 
            this.colorStripMenuItem.Name = "colorStripMenuItem";
            this.colorStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.colorStripMenuItem.Text = "Color scheme";
            this.colorStripMenuItem.Click += new System.EventHandler(this.ColorMenu);
            // 
            // hotKeysStripButton
            // 
            this.hotKeysStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.hotKeysStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.hotKeysStripButton.Name = "hotKeysStripButton";
            this.hotKeysStripButton.Size = new System.Drawing.Size(57, 22);
            this.hotKeysStripButton.Text = "Hot keys";
            this.hotKeysStripButton.Click += new System.EventHandler(this.HotKeysBox);
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.tabControl.Location = new System.Drawing.Point(0, 28);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(817, 495);
            this.tabControl.TabIndex = 1;
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            // 
            // timer
            // 
            this.timer.Tick += new System.EventHandler(this.Timer_Tick);
            // 
            // selectContextStripMenuItem
            // 
            this.selectContextStripMenuItem.Name = "selectContextStripMenuItem";
            this.selectContextStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.selectContextStripMenuItem.Text = "Select all";
            // 
            // cutContextStripMenuItem
            // 
            this.cutContextStripMenuItem.Name = "cutContextStripMenuItem";
            this.cutContextStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.cutContextStripMenuItem.Text = "Cut";
            // 
            // copyContextStripMenuItem
            // 
            this.copyContextStripMenuItem.Name = "copyContextStripMenuItem";
            this.copyContextStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.copyContextStripMenuItem.Text = "Copy";
            // 
            // pasteContextStripMenuItem
            // 
            this.pasteContextStripMenuItem.Name = "pasteContextStripMenuItem";
            this.pasteContextStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.pasteContextStripMenuItem.Text = "Paste";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(143, 6);
            // 
            // italicContextStripMenuItem
            // 
            this.italicContextStripMenuItem.Name = "italicContextStripMenuItem";
            this.italicContextStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.italicContextStripMenuItem.Text = "Italic";
            // 
            // boldContextStripMenuItem
            // 
            this.boldContextStripMenuItem.Name = "boldContextStripMenuItem";
            this.boldContextStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.boldContextStripMenuItem.Text = "Bold";
            // 
            // underlineContextStripMenuItem
            // 
            this.underlineContextStripMenuItem.Name = "underlineContextStripMenuItem";
            this.underlineContextStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.underlineContextStripMenuItem.Text = "Underline";
            // 
            // strikethroughContextStripMenuItem
            // 
            this.strikethroughContextStripMenuItem.Name = "strikethroughContextStripMenuItem";
            this.strikethroughContextStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.strikethroughContextStripMenuItem.Text = "Strikethrough";
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectContextStripMenuItem,
            this.cutContextStripMenuItem,
            this.copyContextStripMenuItem,
            this.pasteContextStripMenuItem,
            this.toolStripSeparator2,
            this.italicContextStripMenuItem,
            this.boldContextStripMenuItem,
            this.underlineContextStripMenuItem,
            this.strikethroughContextStripMenuItem});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(147, 186);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(810, 516);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.upToolStrip);
            this.KeyPreview = true;
            this.Name = "MainForm";
            this.Text = "PrettyTextEditor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.upToolStrip.ResumeLayout(false);
            this.upToolStrip.PerformLayout();
            this.contextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip upToolStrip;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.ToolStripDropDownButton fileStripDropDownButton;
        private System.Windows.Forms.ToolStripDropDownButton editStripDropDownButton;
        private System.Windows.Forms.ToolStripDropDownButton formatStripDropDownButton;
        private System.Windows.Forms.ToolStripButton hotKeysStripButton;
        private System.Windows.Forms.ToolStripMenuItem newStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cutStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem italicStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem boldStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem underlineStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem strikethroughStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem fontStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.ToolStripDropDownButton settingsStripDropDownButton;
        private System.Windows.Forms.ToolStripMenuItem autoSaveToolStripMenu;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.ToolStripMenuItem colorStripMenuItem;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.ToolStripMenuItem selectContextStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cutContextStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyContextStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteContextStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem italicContextStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem boldContextStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem underlineContextStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem strikethroughContextStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
    }
}

