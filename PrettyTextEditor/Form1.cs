﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PrettyTextEditor
{

    public partial class MainForm : Form
    {
        /// <summary>
        /// A list with pathes to files for each tab.
        /// </summary>
        List<string> pathes;
        /// <summary>
        /// The list with boxes of all tabs.
        /// </summary>
        List<RichTextBox> richTextBoxes;
        /// <summary>
        /// Shows if each tab has unsaved changes.
        /// </summary>
        List<bool> edited;
        /// <summary>
        /// Global variable to save unsaved tabs.
        /// </summary>
        int currentIndex = -1;
        /// <summary>
        /// The name of the file with all settings.
        /// </summary>
        string settingsPath = "settings.pte";
        private Form autoSaveForm;
        private ComboBox sizeBox;
        private ComboBox fontBox;
        private ComboBox intervalBox;
        private Form colorForm;
        private Button backColorButton;
        private Form fontForm;
        private Button fontColorButton;
        private Button panelColorButton;
        /// <summary>
        /// Initialize components.
        /// </summary>
        public MainForm()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Loads the main form.
        /// </summary>
        private void MainForm_Load(object sender, EventArgs e)
        {
            // Initializes variables.
            richTextBoxes = new List<RichTextBox>();
            pathes = new List<string>();
            edited = new List<bool>();
            selectContextStripMenuItem.Click += EditText(r => r.SelectAll());
            selectStripMenuItem.Click += EditText(r => r.SelectAll());
            copyContextStripMenuItem.Click += EditText(r => r.Copy());
            copyStripMenuItem.Click += EditText(r => r.Copy());
            pasteContextStripMenuItem.Click += EditText(r => r.Paste());
            pasteStripMenuItem.Click += EditText(r => r.Paste());
            cutContextStripMenuItem.Click += EditText(r => r.Cut());
            cutStripMenuItem.Click += EditText(r => r.Cut());
            boldStripMenuItem.Click += FormatText(FontStyle.Bold);
            boldContextStripMenuItem.Click += FormatText(FontStyle.Bold);
            italicStripMenuItem.Click += FormatText(FontStyle.Bold);
            italicContextStripMenuItem.Click += FormatText(FontStyle.Bold);
            strikethroughStripMenuItem.Click += FormatText(FontStyle.Bold);
            strikethroughContextStripMenuItem.Click += FormatText(FontStyle.Bold);
            underlineStripMenuItem.Click += FormatText(FontStyle.Bold);
            underlineContextStripMenuItem.Click += FormatText(FontStyle.Bold);
            // Opens the first tab.
            NewFile(sender, e);
            // Loads the settings.
            try
            {
                string[] lines = File.ReadAllLines(settingsPath);
                if (int.Parse(lines[0]) != 0)
                {
                    timer.Interval = int.Parse(lines[0]);
                    timer.Enabled = true;
                }
                upToolStrip.BackColor = System.Drawing.ColorTranslator.FromHtml(lines[1]);
                richTextBoxes[0].BackColor = System.Drawing.ColorTranslator.FromHtml(lines[2]);
                richTextBoxes[0].Font = new Font(lines[4], int.Parse(lines[3]));
                richTextBoxes[0].ForeColor = System.Drawing.ColorTranslator.FromHtml(lines[5]);
            }
            catch
            {
                // When there are any errors, makes a default settings file.
                try
                {
                    File.WriteAllLines(settingsPath,
                        new string[] { "0", "Window", "White", "11", "Arial", "Black" });
                }
                catch (ArgumentException)
                {
                    MessageBox.Show("The settings file path is invalid.", "Error!");
                }
                catch (PathTooLongException)
                {
                    MessageBox.Show("Settings file path is too long. Try again.", "Error!");
                }
                catch (DirectoryNotFoundException)
                {
                    MessageBox.Show("There is no such directory. Try again.", "Error!");
                }
                catch (FileNotFoundException)
                {
                    MessageBox.Show("There is not such file with settings. Try again.", "Error!");
                }
                catch (NotSupportedException)
                {
                    MessageBox.Show("The settings file path format is not supported. Try again.", "Error!");
                }
                catch (IOException)
                {
                    MessageBox.Show("IO error. Maybe the settings file has been opened yet. Try again.", "Error!");
                }
                catch (UnauthorizedAccessException)
                {
                    MessageBox.Show("The settings file is available only to read. Try again.", "Error!");
                }
                catch
                {
                    MessageBox.Show("Something went wrong with settings file. Try again.", "Error!");
                }
            }
        }
        /// <summary>
        /// Opens a new tab with a new file.
        /// </summary>
        private void NewFile(object sender, EventArgs e)
        {
            // Creates a new tab.
            string tabTitle = $"Untitled{tabControl.TabCount + 1}.txt";
            TabPage newTab = new TabPage(tabTitle);
            tabControl.TabPages.Add(newTab);
            // Makes the new tab active.
            tabControl.SelectedIndex = tabControl.TabCount - 1;
            // Creates a new RichTextBox.
            var newBox = new RichTextBox();
            richTextBoxes.Add(newBox);
            newBox.Parent = newTab;
            newBox.Location = new Point(0, 0);
            newBox.Width = newTab.Width - 5;
            newBox.Height = newTab.Height - 5;
            newBox.Anchor = ((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left) | AnchorStyles.Right;
            newBox.Name = "richTextBox" + tabControl.TabCount;
            newBox.ContextMenuStrip = contextMenuStrip;
            edited.Add(true);
            pathes.Add(null);
        }
        /// <summary>
        /// Opens a new file in a new tab.
        /// </summary>
        private void OpenFile(object sender, EventArgs e)
        {
            openFileDialog.DefaultExt = "txt";
            openFileDialog.Filter = "TXT (Without formatting)|*.txt|RTF (With formatting)|*.rtf";
            openFileDialog.RestoreDirectory = true;
            try
            {
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    string path = openFileDialog.FileName;
                    FileInfo file = new FileInfo(path);
                    // Opens a new tab.
                    NewFile(sender, e);
                    int index = tabControl.SelectedIndex;
                    if (file.Extension == ".rtf")
                    {
                        // Loads a file with formatting.
                        richTextBoxes[index].LoadFile(path);
                    }
                    else
                    {
                        // Loads a file without formatting.
                        richTextBoxes[index].Text = File.ReadAllText(path);
                    }
                    tabControl.TabPages[index].Text = file.Name;
                }
            }
            catch (ArgumentNullException)
            {
                MessageBox.Show("The file path is null. Try again.", "Error!");
            }
            catch (ArgumentException)
            {
                MessageBox.Show("The file path is invalid. Try again.", "Error!");
            }
            catch (PathTooLongException)
            {
                MessageBox.Show("File path is too long. Try again.", "Error!");
            }
            catch (DirectoryNotFoundException)
            {
                MessageBox.Show("There is no such directory. Try again.", "Error!");
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("There is not such file. Try again.", "Error!");
            }
            catch (NotSupportedException)
            {
                MessageBox.Show("The path format is not supported. Try again.", "Error!");
            }
            catch (IOException)
            {
                MessageBox.Show("IO error. Maybe the file has been opened yet. Try again.", "Error!");
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("The file is available only to read. Try again.", "Error!");
            }
            catch
            {
                MessageBox.Show("Something went wrong. Try again.", "Error!");
            }
        }
        /// <summary>
        /// Save the file with currentIndex.
        /// </summary>
        private void SaveFile(object sender, EventArgs e)
        {
            int index = currentIndex;
            if (currentIndex == -1)
            {
                // Saves the selected tab.
                index = tabControl.SelectedIndex;
            }
            if (pathes[index] == null)
            {
                // If we don't know where should we save the file we open a saveFileDialog.
                SaveAs(sender, e);
                return;
            }
            try
            {
                FileInfo file = new FileInfo(pathes[index]);
                if (file.Extension == ".rtf")
                {
                    // Saves a file with formatting.
                    richTextBoxes[index].SaveFile(pathes[index]);
                }
                else
                {
                    // Saves a file without formatting.
                    File.WriteAllText(pathes[index], richTextBoxes[index].Text);
                }
            }
            catch (ArgumentNullException)
            {
                MessageBox.Show("The file path is null. Try again.", "Error!");
            }
            catch (ArgumentException)
            {
                MessageBox.Show("The file path is invalid. Try again.", "Error!");
            }
            catch (PathTooLongException)
            {
                MessageBox.Show("File path is too long. Try again.", "Error!");
            }
            catch (DirectoryNotFoundException)
            {
                MessageBox.Show("There is no such directory. Try again.", "Error!");
            }
            catch (IOException)
            {
                MessageBox.Show("IO error. Maybe the file has been opened yet. Try again.", "Error!");
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("The file is available only to read. Try again.", "Error!");
            }
            catch (NotSupportedException)
            {
                MessageBox.Show("The format of path is not supported. Try again.", "Error!");
            }
            catch (SecurityException)
            {
                MessageBox.Show("You haven't got enough permissions to write this file. Try again.", "Error!");
            }
            catch
            {
                MessageBox.Show("Something went wrong. Try again.", "Error!");
            }
            edited[index] = false;
        }
        /// <summary>
        /// Opens a saveFileDialog and asks user where to save the file.
        /// </summary>
        private void SaveAs(object sender, EventArgs e)
        {
            if (currentIndex == -1)
            {
                currentIndex = tabControl.SelectedIndex;
            }
            saveFileDialog.RestoreDirectory = true;
            saveFileDialog.FileName = tabControl.TabPages[currentIndex].Text;
            saveFileDialog.DefaultExt = "txt";
            saveFileDialog.Filter = "TXT (Without formatting)|*.txt|RTF (With formatting)|*.rtf";
            try
            {
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    string path = saveFileDialog.FileName;
                    FileInfo file = new FileInfo(path);
                    if (file.Extension == ".rtf")
                    {
                        // Saves a file with formatting.
                        richTextBoxes[currentIndex].SaveFile(path);
                    }
                    else
                    {
                        // Saves a file without formatting.
                        File.WriteAllText(path, richTextBoxes[currentIndex].Text);
                    }
                    pathes[currentIndex] = path;
                    edited[currentIndex] = false;
                    tabControl.TabPages[currentIndex].Text = file.Name;
                }
            }
            catch (ArgumentNullException)
            {
                MessageBox.Show("The file path is null. Try again.", "Error!");
            }
            catch (ArgumentException) {
                MessageBox.Show("The file path is invalid. Try again.", "Error!");
            }
            catch (PathTooLongException)
            {
                MessageBox.Show("File path is too long. Try again.", "Error!");
            }
            catch (DirectoryNotFoundException)
            {
                MessageBox.Show("There is no such directory. Try again.", "Error!");
            }
            catch (IOException)
            {
                MessageBox.Show("IO error. Maybe the file has been opened yet. Try again.", "Error!");
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("The file is available only to read. Try again.", "Error!");
            }
            catch (NotSupportedException)
            {
                MessageBox.Show("The format of path is not supported. Try again.", "Error!");
            }
            catch (SecurityException)
            {
                MessageBox.Show("You haven't got enough permissions to write this file. Try again.", "Error!");
            }
            catch
            {
                MessageBox.Show("Something went wrong. Try again.", "Error!");
            }
        }
        /// <summary>
        /// Runs when the form is closing.
        /// </summary>
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            // If there are some unsaved changes...
            if (Any(edited))
            {
                var result = MessageBox.Show("There are unsaved changes! Do you want to save them?", "Attention!",
                    MessageBoxButtons.YesNoCancel,
                    MessageBoxIcon.Question);
                // Different actions in relation to user's answer.
                switch (result)
                {
                    case DialogResult.Yes:
                        // Saves all files one by one.
                        for (int i = 0; i < edited.Count; ++i)
                        {
                            if (edited[i])
                            {
                                currentIndex = i;
                                SaveFile(sender, e);
                            }
                        }
                        break;
                    case DialogResult.No:
                        break;
                    case DialogResult.Cancel:
                        e.Cancel = true;
                        break;
                }
            }
        }
        /// <summary>
        /// Returns if there are any "true" in the bool list.
        /// </summary>
        /// <param name="list">The list with bool values.</param>
        /// <returns>True is there are any true in the list.</returns>
        bool Any(List<bool> list)
        {
            bool res = false;
            foreach (var elem in list)
            {
                if (elem)
                {
                    res = true;
                }
            }
            return res;
        }
        /// <summary>
        /// Sets different intervals to autosaving.
        /// </summary>
        private void OkButton_Click(object sender, EventArgs e)
        {
            int interval = 0;
            switch (intervalBox.SelectedIndex)
            {
                case 0:
                    timer.Enabled = false;
                    break;
                case 1:
                    interval = 1000;
                    break;
                case 2:
                    interval = 10000;
                    break;
                case 3:
                    interval = 30000;
                    break;
                case 4:
                    interval = 60000;
                    break;
                case 5:
                    interval = 300000;
                    break;
            }
            if (interval != 0)
            {
                timer.Interval = interval;
            }
            // Writes the information to the settings file.
            try
            {
                string[] lines = File.ReadAllLines(settingsPath);
                lines[0] = $"{interval}";
                File.WriteAllLines(settingsPath, lines);
            }
            catch
            {
                // If there are any errors makes a default file.
                try
                {
                    File.WriteAllLines(settingsPath,
                        new string[] { $"{interval}", "Window", "White", "11", "Arial", "Black" });
                }

                catch (ArgumentException)
                {
                    MessageBox.Show("The settings file path is invalid.", "Error!");
                }
                catch (PathTooLongException)
                {
                    MessageBox.Show("Settings file path is too long. Try again.", "Error!");
                }
                catch (DirectoryNotFoundException)
                {
                    MessageBox.Show("There is no such directory. Try again.", "Error!");
                }
                catch (FileNotFoundException)
                {
                    MessageBox.Show("There is not such file with settings. Try again.", "Error!");
                }
                catch (NotSupportedException)
                {
                    MessageBox.Show("The settings file path format is not supported. Try again.", "Error!");
                }
                catch (IOException)
                {
                    MessageBox.Show("IO error. Maybe the settings file has been opened yet. Try again.", "Error!");
                }
                catch (UnauthorizedAccessException)
                {
                    MessageBox.Show("The settings file is available only to read. Try again.", "Error!");
                }
                catch
                {
                    MessageBox.Show("Something went wrong with settings file. Try again.", "Error!");
                }
            }
            timer.Enabled = (interval != 0);
            autoSaveForm.Close();
        }
        /// <summary>
        /// Autosaving.
        /// </summary>
        private void Timer_Tick(object sender, EventArgs e)
        {
            timer.Enabled = false;
            foreach (string path in pathes)
            {
                if (path == null)
                {
                    MessageBox.Show("There are some unsaved files. Please specify the path to save them.", "Autosaving!");
                    break;
                }
            }
            // Saves all files.
            for (currentIndex = 0; currentIndex < tabControl.TabCount; ++currentIndex)
            {
                SaveFile(sender, e);
            }
            timer.Enabled = true;
        }
        /// <summary>
        /// Opens coloDialog to choose the background color.
        /// </summary>
        private void BackColorButton_Click(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                backColorButton.BackColor = colorDialog.Color;
            }
        }
        /// <summary>
        /// Opens colorDialog to choose the panel color.
        /// </summary>
        private void PanelColorButton_Click(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                panelColorButton.BackColor = colorDialog.Color;
            }
        }
        /// <summary>
        /// Changes the color scheme and saves it.
        /// </summary>
        private void OkColorButton_Click(object sender, EventArgs e)
        {
            // Changes color of the panel.
            upToolStrip.BackColor = panelColorButton.BackColor;
            // Changes color of all richTextBoxes.
            foreach (var richTextBox in richTextBoxes)
            {
                richTextBox.BackColor = backColorButton.BackColor;
            }
            // Saves the settings.
            try
            {
                string[] lines = File.ReadAllLines(settingsPath);
                lines[1] = $"{ColorTranslator.ToHtml(panelColorButton.BackColor)}";
                lines[2] = $"{ColorTranslator.ToHtml(backColorButton.BackColor)}";
                File.WriteAllLines(settingsPath, lines);
            }
            catch
            {
                // If there are any errors makes a default file.
                try
                {
                    File.WriteAllLines(settingsPath,
                        new string[] { "0", $"{ColorTranslator.ToHtml(panelColorButton.BackColor)}",
                        $"{ColorTranslator.ToHtml(backColorButton.BackColor)}",
                    "11", "Arial", "Black"});
                }

                catch (ArgumentException)
                {
                    MessageBox.Show("The settings file path is invalid.", "Error!");
                }
                catch (PathTooLongException)
                {
                    MessageBox.Show("Settings file path is too long. Try again.", "Error!");
                }
                catch (DirectoryNotFoundException)
                {
                    MessageBox.Show("There is no such directory. Try again.", "Error!");
                }
                catch (FileNotFoundException)
                {
                    MessageBox.Show("There is not such file with settings. Try again.", "Error!");
                }
                catch (NotSupportedException)
                {
                    MessageBox.Show("The settings file path format is not supported. Try again.", "Error!");
                }
                catch (IOException)
                {
                    MessageBox.Show("IO error. Maybe the settings file has been opened yet. Try again.", "Error!");
                }
                catch (UnauthorizedAccessException)
                {
                    MessageBox.Show("The settings file is available only to read. Try again.", "Error!");
                }
                catch
                {
                    MessageBox.Show("Something went wrong with settings file. Try again.", "Error!");
                }
            }
            colorForm.Close();
        }
        /// <summary>
        /// Returns handler to use different font styles.
        /// </summary>
        private EventHandler FormatText(FontStyle fontStyle) {
            return new EventHandler((object sender, EventArgs e) =>
            {
                var richTextBox = richTextBoxes[tabControl.SelectedIndex];
                richTextBox.SelectionFont = new Font(richTextBox.SelectionFont, richTextBox.SelectionFont.Style | fontStyle);
            });
        }
        /// <summary>
        /// A deligate to use different operations with richTextBox.
        /// </summary>
        delegate void EditDelegate(RichTextBox richTextBox);
        /// <summary>
        ///  Returns handler to use different operations with richTextBox.
        /// </summary>
        /// <param name="operation">The operation that you want to use to the richTextBox.</param>
        private EventHandler EditText(EditDelegate operation)
        {
            return new EventHandler((object sender, EventArgs e) =>
            {
                var richTextBox = richTextBoxes[tabControl.SelectedIndex];
                operation(richTextBox);
            });
        }
        /// <summary>
        /// Opens a colorDialog and changes the color.
        /// </summary>
        private void FontColorButton_Click(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                fontColorButton.ForeColor = colorDialog.Color;
            }
        }
        /// <summary>
        /// Changes the font and saves into the settings file.
        /// </summary>
        private void OkFontButton_Click(object sender, EventArgs e)
        {
            int size;
            // If user has chosen any size...
            if (sizeBox.Text != "")
            {
                size = int.Parse(sizeBox.Text);
            }
            else
            {
                size = (int)richTextBoxes[0].Font.Size;
            }
            string font;
            // If user has chosen any font...
            if (fontBox.Text != "")
            {
                font = fontBox.Text;
            }
            else
            {
                font = richTextBoxes[0].Font.Name;
            }
            // Changes the font for all tabs.
            for (int i = 0; i < tabControl.TabCount; ++i)
            {
                richTextBoxes[i].Font = new Font(font, size);
                richTextBoxes[i].ForeColor = fontColorButton.ForeColor;
            }
            fontForm.Close();
            // Saves the new font to the settings file.
            try
            {
                string[] lines = File.ReadAllLines(settingsPath);
                lines[3] = $"{size}";
                lines[4] = font;
                lines[5] = $"{ColorTranslator.ToHtml(fontColorButton.ForeColor)}";
                File.WriteAllLines(settingsPath, lines);
            }
            catch
            {
                // If there are any errors makes a default sttings file.
                try
                {
                    File.WriteAllLines(settingsPath,
                        new string[] { "0", "Window", "White", $"{size}", font,
                        $"{ColorTranslator.ToHtml(fontColorButton.ForeColor)}" });
                }

                catch (ArgumentException)
                {
                    MessageBox.Show("The settings file path is invalid.", "Error!");
                }
                catch (PathTooLongException)
                {
                    MessageBox.Show("Settings file path is too long. Try again.", "Error!");
                }
                catch (DirectoryNotFoundException)
                {
                    MessageBox.Show("There is no such directory. Try again.", "Error!");
                }
                catch (FileNotFoundException)
                {
                    MessageBox.Show("There is not such file with settings. Try again.", "Error!");
                }
                catch (NotSupportedException)
                {
                    MessageBox.Show("The settings file path format is not supported. Try again.", "Error!");
                }
                catch (IOException)
                {
                    MessageBox.Show("IO error. Maybe the settings file has been opened yet. Try again.", "Error!");
                }
                catch (UnauthorizedAccessException)
                {
                    MessageBox.Show("The settings file is available only to read. Try again.", "Error!");
                }
                catch
                {
                    MessageBox.Show("Something went wrong with settings file. Try again.", "Error!");
                }
            }
        }
        /// <summary>
        /// Opens colorDialog to choose the panel color.
        /// </summary>
        private void HotKeysBox(object sender, EventArgs e)
        {
            MessageBox.Show(@"Ctrl+Shift+N opens new window
Ctrl+N opens new tab
Ctrl+Shif+S saves all opened files
Ctrl+S saves the selected file
Ctrl+Q quites the application", "The list of all hot keys:");
        }
        /// <summary>
        /// Processing of hot keys.
        /// </summary>
        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control)
            {
                switch (e.KeyCode)
                {
                    case Keys.N:
                        if (e.Shift)
                        {
                            // Ctrl+Shif+N.
                            // Opens new window.
                            using (Process myProcess = new Process())
                            {
                                myProcess.StartInfo.UseShellExecute = false;
                                // Starts the program again.
                                myProcess.StartInfo.FileName = Process.GetCurrentProcess().MainModule.FileName;
                                myProcess.Start();
                            }
                        }
                        else
                        {
                            // Ctrl+N.
                            // Opens new file.
                            NewFile(sender, e);
                        }
                        break;
                    case Keys.S:
                        if (e.Shift)
                        {
                            // Ctrl+Shif+S.
                            // Saves all opened files.
                            for (currentIndex = 0; currentIndex < tabControl.TabCount; ++currentIndex)
                            {
                                SaveFile(sender, e);
                            }
                        }
                        else
                        {
                            // Ctrl+S.
                            // Saves only the selected file.
                            currentIndex = -1;
                            SaveFile(sender, e);
                        }
                        break;
                    case Keys.Q:
                        // Ctrl+Q.
                        // Quites the aapplication.
                        Close();
                        break;
                }
                e.SuppressKeyPress = true;
            }
        }

        private void AutoSaveMenu(Object sender, EventArgs e)
        {
            autoSaveForm = new Form();
            autoSaveForm.Text = "Auto saving";
            autoSaveForm.Width = 300;
            autoSaveForm.Height = 115;
            Label autoSaveLabel = new Label();
            autoSaveLabel.Width = autoSaveForm.Width;
            autoSaveLabel.Anchor = AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Left;
            autoSaveLabel.Parent = autoSaveForm;
            autoSaveLabel.Location = new Point(0, 0);
            autoSaveLabel.Text = "Choose autosaving interval:";
            autoSaveLabel.Font = new Font("Arial", 15);
            intervalBox = new ComboBox();
            intervalBox.Parent = autoSaveForm;
            intervalBox.Items.AddRange(new string[] 
            { "Autosaving off",
            "1 second",
            "10 seconds",
            "1 minute",
            "5 minutes"});
            intervalBox.Location = new Point(0, 25);
            Button okAutoSaveButton = new Button();
            okAutoSaveButton.Width = autoSaveForm.Width;
            okAutoSaveButton.Parent = autoSaveForm;
            okAutoSaveButton.Location = new Point(0, 55);
            okAutoSaveButton.Text = "Ok";
            okAutoSaveButton.Click += OkButton_Click;
            okAutoSaveButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Left;
            autoSaveForm.Show();
        }

        private void ColorMenu(Object sender, EventArgs e)
        {
            colorForm = new Form();
            colorForm.Text = "Color scheme";
            colorForm.Width = 300;
            colorForm.Height = 120;
            panelColorButton = new Button();
            panelColorButton.Parent = colorForm;
            panelColorButton.Text = "Choose upper panel color";
            panelColorButton.BackColor = upToolStrip.BackColor;
            panelColorButton.Width = 300;
            panelColorButton.Anchor = AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Left;
            panelColorButton.Click += PanelColorButton_Click;
            backColorButton = new Button();
            backColorButton.Parent = colorForm;
            backColorButton.Width = 300;
            backColorButton.Location = new Point(0, 30);
            backColorButton.BackColor = richTextBoxes[0].BackColor;
            backColorButton.Text = "Choose back color";
            backColorButton.Click += BackColorButton_Click;
            backColorButton.Anchor = AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Left;
            Button okColorButton = new Button();
            okColorButton.Parent = colorForm;
            okColorButton.Width = 300;
            okColorButton.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Right | AnchorStyles.Left;
            okColorButton.Location = new Point(0, 60);
            okColorButton.Click += OkColorButton_Click;
            okColorButton.Text = "Ok";
            colorForm.Show();
        }

        private void FontMenu(Object sender, EventArgs e)
        {
            fontForm = new Form();
            fontForm.Text = "Font settings";
            fontForm.Width = 300;
            fontForm.Height = 185;
            Label sizeLabel = new Label();
            sizeLabel.Parent = fontForm;
            sizeLabel.Text = "Choose the font size:";
            sizeLabel.Font = new Font("Arial", 15);
            sizeLabel.Location = new Point(0, 0);
            sizeLabel.Width = 300;
            sizeBox = new ComboBox();
            sizeBox.Parent = fontForm;
            for (int i = 11; i < 100; i += 2)
            {
                sizeBox.Items.Add(i);
            }
            sizeBox.Location = new Point(0, 25);
            Label fontLabel = new Label();
            fontLabel.Width = 300;
            fontLabel.Parent = fontForm;
            fontLabel.Text = "Choose the font:";
            fontLabel.Font = new Font("Arial", 15);
            fontLabel.Location = new Point(0, 50);
            fontBox = new ComboBox();
            fontBox.Parent = fontForm;
            fontBox.Location = new Point(0, 75);
            var installedFontCollection = new System.Drawing.Text.InstalledFontCollection();
            foreach (var x in installedFontCollection.Families)
            {
                fontBox.Items.Add(x.Name);
            }
            fontColorButton = new Button();
            fontColorButton.Location = new Point(0, 100);
            fontColorButton.Text = "Choose the font color";
            fontColorButton.Parent = fontForm;
            fontColorButton.ForeColor = richTextBoxes[0].ForeColor;
            fontColorButton.Click += FontColorButton_Click;
            fontColorButton.Width = 300;
            fontColorButton.Anchor = AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Left;
            Button okFontButton = new Button();
            okFontButton.Parent = fontForm;
            okFontButton.Location = new Point(0, 125);
            okFontButton.Width = fontForm.Width;
            okFontButton.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Right | AnchorStyles.Left;
            okFontButton.Click += OkFontButton_Click;
            okFontButton.Text = "Ok";
            fontForm.Show();
        }
    }
}
